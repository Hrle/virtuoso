#!/usr/bin/env sh

sudo umount "$2"*
sudo mkfs.vfat "$2" -I
sudo dd if="$1" of="$2"
