Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
Invoke-RestMethod get.scoop.sh | Invoke-Expression
scoop bucket add "nerd-fonts"
scoop bucket add "extras"

winget install "Docker.DockerDesktop"

winget install "OpenVPNTechnologies.OpenVPN"
winget install "Fortinet.FortiClientVPN"
winget install "GnuPG.Gpg4win"
winget install "gerardog.gsudo"
winget install "Git.Git"

winget install "Python.Python.3"
winget install "OpenJS.NodeJS"
winget install "Yarn.Yarn"
winget install "Microsoft.PowerShell"
winget install "JSFoundation.Appium"
winget install "Rustlang.Rustup"
winget install "RubyInstallerTeam.Ruby.3.1"
winget install "Microsoft.VisualStudio.2022.BuildTools"
winget install "GnuWin32.Make"
winget install "Kitware.CMake"
winget insatll "Amazon.AWSCLI"
winget insatll "Amazon.SAM-CLI"
winget insatll "Postman.Postman"

winget install "Microsoft.SQLServer.2019.Developer"
winget install "dbeaver.dbeaver"
winget install "Microsoft.SQLServerManagementStudio"

winget install "Microsoft.WindowsTerminal"
winget install "Nushell.Nushell"
winget install "vim.vim"
winget install "Neovim.Neovim"
pip install neovim
winget install "Microsoft.VisualStudioCode"
winget install "Microsoft.VisualStudio.2022.Community"

# TODO: add shotwell when it gets added to winget
winget install "TheDocumentFoundation.LibreOffice.LTS"
winget install "Meltytech.Shotcut"
winget install "VideoLAN.VLC"
winget install "OBSProject.OBSStudio"
winget install "Pinta.Pinta"
winget install "Audacity.Audacity"
winget install "Spotify.Spotify"
winget install "Valve.Steam"
winget install "Rufus.Rufus"
winget install "JAMSoftware.TreeSize.Free"

winget install "SyncTrayzor.SyncTrayzor"
winget install "KeePassXCTeam.KeePassXC"
winget install "BraveSoftware.BraveBrowser"
winget install "eloston.ungoogled-chromium"
winget install "AmineMouafik.Ferdi"

scoop install "lazygit"
scoop install "lazydocker"
scoop install "zoxide"
scoop install "starship"
scoop install "ripgrep"
scoop install "sed"
scoop install "fd"
scoop install "jq"
scoop install "fzf"
scoop install "file"
scoop install "dos2unix"
scoop install "alacritty"
scoop install "sqlite"
sudo scoop install -g "JetBrainsMono-NF-Mono"
sudo scoop install -g "JetBrainsMono-NF"

# NOTE: install clang https://stackoverflow.com/a/72458912
