#!/usr/bin/env sh

curl \
  -L "https://gitlab.com/Hrle/virtuoso/-/archive/main/virtuoso-main.tar.gz" |
  tar -xz
mv virtuoso-main virtuoso
cd virtuoso || exit
