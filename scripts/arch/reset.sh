#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Resetting device...\n"

if [ ! "$(id -un)" = "root" ]; then
  printf "[VIRTUOSO]: Please run this as root\n"
fi

DEVICE=$1
if ! echo "$DEVICE" | grep -Eq "/dev/.*"; then
  printf "[VIRTUOSO]: Please enter a valid device for the first argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s device\n" "$DEVICE"

if [ ! -b "$DEVICE" ]; then
  printf "[VIRTUOSO]: The entered device is not a block device\n"
  exit 1
fi

part() {
  if echo "$1" | grep -q "nvme"; then
    echo "$1p$2"
  else
    echo "$1$2"
  fi
}

umount /mnt/boot
umount /mnt/var/log
umount /mnt/.snapshots
umount /mnt/home/*/.snapshots
umount /mnt/home/*
umount /mnt
swapoff "$(part "$DEVICE" 2)"
parted "$DEVICE" mktable gpt
parted "$DEVICE" print

printf "\n[VIRTUOSO]: Device reset\n"
