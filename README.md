# Virtuoso

Virtuoso is a collection of scripts for setting up virtual machines and
installing operating systems.

TOOD: use pyinfra

## HOWTO

### Clone (easiest)

```shell

```

### Download using `CURL`

```shell
cd /opt;
LINK="https://gitlab.com/Hrle/virtuoso/-/archive/main/virtuoso-main.tar.gz";
curl -L "$LINK" | tar -xz;
mv virtuoso-main virtuoso;
cd virtuoso;
```

### Mount inside `VM`

```shell
mkdir /opt/virtuoso;
sudo mount -t 9p -o trans=virtio virtuoso /opt/virtuoso
cd /opt/virtuoso;
```

## Arch

Some tricks to get arch working.

### NVidia power management

You have to have a compatible card for this and it is experimental.
Here is how you can preserve video memory on sleep and hibernate.

Write this in `/etc/modprobe.d/nvidia-power-management.conf`:

```txt
options nvidia NVreg_PreserveVideoMemoryAllocations=1
options nvidia NVreg_TemporaryFilePath=/.vswap/swapfile
```

Enable the `nvidia-suspend` and `nvidia-hibernate` services.

Read the [wiki](https://wiki.archlinux.org/title/NVIDIA/Tips_and_tricks#Preserve_video_memory_after_suspend)
and [driver README](https: //download.nvidia.com/XFree86/Linux-x86_64/495.44/README/powermanagement.html)
for more information.

### LibreOffice Dark Mode

Tools ->
Options ->
Application Colors ->
Document background & Application

### `Omnisharp` fix

[Fix](https://stackoverflow.com/questions/67209554/omnisharp-failure-cant-create-task-getreferencenearesttargetframeworktask).

Command:

```shell
ls /usr/lib/mono/gac/ | grep v4 | xargs -I R sudo rm -rf /usr/lib/mono/gac/R
ls /usr/lib/mono/gac/Microsoft.Build.Framework/ | grep "^4.0.0" | xargs -I R sudo rm -rf /usr/lib/mono/gac/Microsoft.Build.Framework/R
```

### Dual boot

#### Time zone

Arch Linux recommends setting the time zone of both systems to UTC.

On Arch Linux:

```shell
timedatectl set-timezone UTC
```

On Windows:

```pwsh
Set-TimeZone -Id "UTC"
```

#### Secure boot

Some programs on Windows require secure boot.

#### Hibernation and Fast startup

Fast startup is just automatic hibernation to make Windows seem like it is
booting very fast.

Disable fast startup and hibernation because mounting and reading or writing can
lead to data loss on partitions which OS' have been hibernated. In the case of
Arch Linux, this is impossible to mitigate on a dual-boot system with `GRUB` as
`os-prober` always tries to mount and read the Windows partition on `GRUB`
updates. This includes when GRUB refreshes the configuration during snapshotting
which happens any time you install and/or update a package.

- TODO: try and run `os-prober` just once when Windows is not hibernated and
  disable it afterwards in `/etc/default/grub`

Other than this, using separate `ESP's` for Linux and Windows is also required
because mounting/reading/writing those can also cause data loss.

- NOTE: `ntfs-3g` has a safeguard for mounting hibernated `NTFS` partitions, but
  the kernel doesn't

#### Evolution EWS

Set Host URL to
[Office 365](https://outlook.office365.com/EWS/Exchange.asmx).
Set authentication type to OAuth2.
Override OAuth2 settings and set Tenant ID from
[Azure Active Directory Portal](https://aad.portal.azure.com/)

### Things to do after install

- check the logs
- systemctl reboot
- run the reboot.sh script
- configure qtile hardware ids (/etc/profile.d/qtile.sh)
- configure syncthing
- configure keepassxc
- configure radicale
- configure evolution
- configure ferdium
- configure brave
- enable chrome://flags/#allow-insecure-localhost in chromium and brave
- configure ssh, gpg, fingerprint
- configure firewall (transmission 51413, radicale 5232, aspnetcore 5000/5001, stable diffusion 7860, tavern ai 8000)
- betterlockscreen --update {wallpaper}

### Over SSH

First do this. On Windows use `type` instead of `cat`.

```shell
cat "$public_key" | ssh "$hostname" "cat >> .ssh/authorized_keys"
```
