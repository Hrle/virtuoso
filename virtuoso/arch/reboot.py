from virtuoso.arch.deploy.packages.install_terminal_tools import (
    install_terminal_tools,
)
from virtuoso.arch.deploy.packages.install_dev_tools import install_dev_tools
from virtuoso.arch.deploy.packages.install_media_tools import (
    install_media_tools,
)
from virtuoso.arch.deploy.packages.install_apps import install_apps
from virtuoso.arch.deploy.packages.install_virtualization_services_and_tools import (
    install_virtualization_services_and_tools,
)
from virtuoso.arch.deploy.cleanup import cleanup

install_terminal_tools()
install_dev_tools()
install_media_tools()
install_apps()
install_virtualization_services_and_tools()

cleanup()
