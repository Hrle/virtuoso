from virtuoso.arch.deploy.configure_dns import configure_dns
from virtuoso.arch.deploy.configure_mirrors import configure_mirrors
from virtuoso.arch.deploy.bootstrap import bootstrap
from virtuoso.arch.deploy.configure_repositories import configure_repositories
from virtuoso.arch.deploy.configure_user import configure_user
from virtuoso.arch.deploy.setup_bootloader import setup_bootloader
from virtuoso.arch.deploy.configure_locale import configure_locale
from virtuoso.arch.deploy.setup_base_packages import setup_base_packages
from virtuoso.arch.deploy.configure_themes import configure_themes

configure_mirrors()
bootstrap()
configure_dns()
configure_repositories()
setup_bootloader()
configure_user()
configure_locale()
setup_base_packages()
configure_themes()
