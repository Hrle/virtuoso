from pyinfra.api import deploy
from pyinfra.operations import server
from pyinfra import logger
from virtuoso.arch.deploy.helpers import shell_now


@deploy("Configure mirrors")
def configure_mirrors():
    reflector_ran = shell_now("cat /etc/pacman.d/mirrorlist | grep Reflector")
    if reflector_ran and len(reflector_ran) > 0 and reflector_ran[0]:
        logger.info("Mirrors configured earlier")
        return
    else:
        logger.info("Configuring mirrors")

    server.shell(
        name="Install reflector",
        commands="pacman -Syy --noconfirm --disable-download-timeout \
          arch-install-scripts \
          archlinux-keyring \
          pacman-mirrorlist \
          python \
          reflector",
    )

    server.shell(
        name="Run reflector",
        commands="reflector \
          --latest 10 \
          --sort rate \
          --save /etc/pacman.d/mirrorlist",
    )
