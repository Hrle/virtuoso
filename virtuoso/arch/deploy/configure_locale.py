from pyinfra.api import deploy
from pyinfra.operations import files, server
from pyinfra import host
from virtuoso.arch.deploy.helpers import chroot_prefix, chroot_shell


@deploy("Configure locale", data_defaults={"zoneinfo": "Europe/Zagreb"})
def configure_locale():
    files.link(
        name="Set timezone",
        path="/etc/localtime",
        target=f"/usr/share/zoneinfo/{host.data.zoneinfo}",
        _shell_executable=chroot_shell(),
    )

    files.put(
        name="Generate locales",
        src="assets/arch/locale.gen",
        dest=chroot_prefix("/etc/locale.gen"),
    )

    files.put(
        name="Set system locale",
        src="assets/arch/locale.conf",
        dest=chroot_prefix("/etc/locale.conf"),
    )

    server.shell(
        name="Generate locale",
        commands="locale-gen",
        _shell_executable=chroot_shell(),
    )
