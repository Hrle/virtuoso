from pyinfra.api import deploy
from pyinfra.operations import server
from pyinfra import logger
from virtuoso.arch.deploy.helpers import is_wsl


@deploy("Install apps")
def install_apps():
    if is_wsl():
        logger.info("Skipping apps install on WSL")
        return

    server.shell(
        f"paru -S --noconfirm --disable-download-timeout \
          {' '.join(LUTRIS_LIBS)}"
    )

    server.shell(
        f"paru -S --noconfirm --disable-download-timeout \
          {' '.join(ONEDRIVE_LIB)}"
    )

    server.shell(
        f"paru -S --noconfirm --disable-download-timeout \
          {' '.join(PACKAGES)}"
    )

    server.shell(
        "onedrive --confdir=~/.config/onedrive/*/*/ --synchronize --verbose"
    )

    for service in SERVICES:
        server.shell(f"systemctl enable --user --now {service}.service")


LUTRIS_LIBS = [
    "wine-staging",
    "giflib",
    "lib32-giflib",
    "libpng",
    "lib32-libpng",
    "libldap",
    "lib32-libldap",
    "gnutls",
    "lib32-gnutls",
    "mpg123",
    "lib32-mpg123",
    "openal",
    "lib32-openal",
    "v4l-utils",
    "lib32-v4l-utils",
    "libpulse",
    "lib32-libpulse",
    "libgpg-error",
    "lib32-libgpg-error",
    "alsa-plugins",
    "lib32-alsa-plugins",
    "alsa-lib",
    "lib32-alsa-lib",
    "libjpeg-turbo",
    "lib32-libjpeg-turbo",
    "sqlite",
    "lib32-sqlite",
    "libxcomposite",
    "lib32-libxcomposite",
    "libxinerama",
    "lib32-libgcrypt",
    "libgcrypt",
    "lib32-libxinerama",
    "ncurses",
    "lib32-ncurses",
    "ocl-icd",
    "lib32-ocl-icd",
    "libxslt",
    "lib32-libxslt",
    "libva",
    "lib32-libva",
    "gtk3",
    "lib32-gtk3",
    "gst-plugins-base-libs",
    "lib32-gst-plugins-base-libs",
    "vulkan-icd-loader",
    "lib32-vulkan-icd-loader",
]

ONEDRIVE_LIB = ["liblphobos"]

PACKAGES = [
    "xdg-user-dirs",
    "handlr",
    "kitty",
    "syncthing",
    "radicale",
    "evolution",
    "evolution-on",
    "evolution-ews",
    "keepassxc",
    "keepmenu-git",
    "python-pynput",
    "seahorse",
    "epdfview",
    "libreoffice-still",
    "libreoffice-still-hr",
    "hunspell",
    "hunspell-hr",
    "libmythes",
    "hyphen",
    "obs-studio",
    "shotcut",
    "pinta",
    "shotwell",
    "audacity",
    "vlc",
    "ferdium-bin",
    "brave-bin",
    "lutris",
    "steam-native-runtime",
    "protontricks-git",
    "winetricks-git",
    "spotifyd",
    "spotify-tui",
    "spotify",
    "transmission-cli",
    "transmission-gtk",
    "emote",
    "popsicle",
    "onedrive-abrounegg",
]

SERVICES = [
    "syncthing",
    "spotifyd",
    "transmission",
    "onedrive-*",
    "radicale",
]
