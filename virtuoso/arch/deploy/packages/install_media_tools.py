from pyinfra.api import deploy
from pyinfra.operations import server
from pyinfra import logger
from virtuoso.arch.deploy.helpers import is_wsl


@deploy("Install media tools")
def install_media_tools():
    if is_wsl():
        logger.info("Skipping media tools install on WSL")
        return

    server.shell(
        f"paru -S --noconfirm --disable-download-timeout \
          {' '.join(PACKAGES)}"
    )


PACKAGES = [
    "feh",
    "imagemagick",
    "ueberzug",
    "tumbler",
    "openjpeg2",
    "libjxl",
    "librsvg",
    "openexr",
    "libwmf",
    "libheif",
    "mpv",
    "libwebp",
    "ffmpeg",
    "ffnvcodec-headers",
    "ffmpegthumbnailer",
    "odt2txt",
    "freetype2",
    "libgsf",
    "gnome-epub-thumbnailer",
    "evince",
    "libxml2",
    "libcaca",
    "djvulibre",
    "ghostscript",
    "pango",
    "poppler",
    "python-chardet",
]
