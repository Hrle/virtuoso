from pyinfra.api import deploy
from pyinfra.operations import files, server


@deploy("Install terminal tools")
def install_terminal_tools():
    server.shell(
        f"paru -S --noconfirm --disable-download-timeout {' '.join(PACKAGES)}"
    )

    files.put(
        name="Create GoDNS service file",
        src="assets/arch/godns.service",
        dest="/lib/systemd/user/godns.service",
    )

    files.put(
        name="Create GoDNS timer file",
        src="assets/arch/godns.timer",
        dest="/lib/systemd/user/godns.timer",
    )

    server.shell("systemctl enable --user --now godns.service")


PACKAGES = [
    "bash-completion",
    "cod",
    "fastfetch",
    "talkfilters",
    "zoxide",
    "bat",
    "exa",
    "tree",
    "ranger",
    "lazygit",
    "git-delta",
    "fd",
    "sd",
    "sad",
    "ripgrep",
    "fzf",
    "fzy",
    "jq",
    "rlwrap",
    "dos2unix",
    "atool",
    "zip",
    "mediainfo",
    "lynx",
    "speedtest-cli",
    "procs",
    "htop",
    "powertop",
    "inxi",
    "duf",
    "ncdu",
    "glow",
    "highlight",
    "zk",
    "unrar",
    "dog",
    "net-tools",
    "godns-bin",
    "renameutils",
    "translate-shell",
    "woeusb",
]
