from pyinfra.api import deploy
from pyinfra.operations import server, files
from pyinfra import logger
from virtuoso.arch.deploy.helpers import is_wsl


@deploy("Install virtualization services and tools")
def install_virtualization_services_and_tools():
    if is_wsl():
        logger.info("Skipping virtualization services and tools install on WSL")
        return

    server.shell(
        f"yes | paru -S --disable-download-timeout {' '.join(PACKAGES)}"
    )

    server.shell("sudo systemctl enable --now libvirtd.service")
    server.shell("sudo systemctl enable --now cockpit.socket")
    server.shell("sudo systemctl enable --now docker.service")
    server.shell("sudo virsh net-autostart default")

    files.put(
        name="Create Elasticsearch config",
        src="assets/arch/elasticsearch.conf",
        dest="/etc/sysctl.d/elasticsearch.conf",
    )

    current_user = server.shell("id -un")
    server.shell(f"sudo usermod -aG libvirt {current_user}")
    server.shell(f"sudo usermod -aG docker {current_user}")


PACKAGES = [
    "docker",
    "docker-compose",
    "lazydocker",
    "libvirt",
    "libguestfs",
    "virt-viewer",
    "virt-install",
    "qemu-desktop",
    "cockpit",
    "cockpit-machines",
    "packagekit",
    "iptables-nft",
    "dnsmasq",
    "dmidecode",
    "bridge-utils",
    "openbsd-netcat",
]
