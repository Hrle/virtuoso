from pyinfra.api import deploy
from pyinfra.operations import files, server
from pyinfra import host
from virtuoso.arch.deploy.helpers import is_wsl, chroot_prefix
from virtuoso.arch.deploy.setup_chroot_shell import setup_chroot_shell


@deploy("Configure DNS", data_defaults={"user": "user"})
def configure_dns():
    if not is_wsl():
        setup_chroot_shell()

    files.put(
        name="Configure nameservers",
        src="assets/arch/resolv.conf",
        dest=chroot_prefix("/etc/resolv.conf"),
    )

    if is_wsl():
        files.template(
            name="Configure WSL",
            src="assets/arch/wsl.conf",
            dest="/etc/wsl.conf",
            wsl_user=host.data.user,
        )

        server.shell("chattr +i /etc/resolv.conf")
