from pyinfra import host
from pyinfra.api import deploy
from pyinfra.operations import files, server
from virtuoso.arch.deploy.helpers import is_wsl, chroot_shell, chroot_prefix
from virtuoso.arch.deploy.setup_chroot_shell import setup_chroot_shell


@deploy(
    "Configure user",
    data_defaults={
        "user": "virtuoso",
        "password": "virtuoso",
        "dotfiles": "https://gitlab.com/virtuoso/dotfiles",
    },
)
def configure_user():
    if not is_wsl():
        setup_chroot_shell()

    server.shell(
        name="Install base user packages",
        commands=f"\
          pacman -S --noconfirm --disable-download-timeout \
            {' '.join(BASE_USER_PACKAGES)}",
        _shell_executable=chroot_shell(),
    )
    files.put(
        name="Sudo wheel group",
        src="assets/arch/wheel",
        dest="/etc/sudoers.d/wheel" if is_wsl() else "/mnt/etc/sudoers.d/wheel",
    )
    files.put(
        name="Doas configuration",
        src="assets/arch/doas.conf",
        dest="/etc/doas.conf" if is_wsl() else "/mnt/etc/doas.conf",
        mode="0400",
    )

    server.shell(
        name="Setup user",
        commands=f'\
            cat "/etc/passwd" | grep -q "{host.data.user}" && \
            echo "User already setup" || \
              ( \
                useradd -s /usr/bin/zsh {host.data.user} && \
                usermod -aG wheel {host.data.user} && \
                echo root:{host.data.password} | chpasswd && \
                echo {host.data.user}:{host.data.password} | chpasswd && \
                echo "User setup" \
              )',
        _shell_executable=chroot_shell(),
    )

    user_directories = [
        f"/home/{host.data.user}",
        f"/opt/src/{host.data.user}/repos",
        f"/home/{host.data.user}/.cache",
        f"/home/{host.data.user}/.cache/zsh",
        f"/home/{host.data.user}/.ssh",
        f"/home/{host.data.user}/.ssh/keys",
    ]
    for user_directory in user_directories:
        files.directory(
            path=user_directory,
            user=host.data.user,
            group=host.data.user,
            _shell_executable=chroot_shell(),
        )

    server.shell(
        name="Configure dotfiles",
        commands=f'\
          su {host.data.user} -c \' \
            ls -a "/home/{host.data.user}" | grep -q ".git" && \
            echo "Dotfiles already configured" || \
              ( \
                cd "/home/{host.data.user}" && \
                git init --initial-branch initial && \
                git remote add readonly "{host.data.dotfiles}" && \
                git fetch readonly && \
                git switch main && \
                git pull readonly main --recurse-submodules && \
                git submodule update --init && \
                echo "Dotfiles configured" \
              )\'',
        _shell_executable=chroot_shell(),
    )

    server.shell(
        name="Enable SSH Daemon",
        commands="systemctl enable sshd",
        _shell_executable=chroot_shell(),
    )
    server.shell(
        name="Generate user SSH key",
        commands=f'\
          su {host.data.user} -c \' \
            ls /home/{host.data.user}/.ssh/keys/id_ed25519 && \
            echo "SSH key already generated" || \
            ( \
              ssh-keygen \
                -a 100 \
                -t ed25519 \
                -C "{host.data.machine}" \
                -N "" \
                -f "/home/{host.data.user}/.ssh/keys/id_ed25519" && \
              echo "SSH key generated" \
            )\'',
        _shell_executable=chroot_shell(),
    )

    files.directory(
        name="Create GPG directory",
        path=chroot_prefix(f"/home/{host.data.user}/.gnupg"),
        user=host.data.user,
        group=host.data.user,
        mode="0700",
        _shell_executable=chroot_shell(),
    )
    files.put(
        name="Configure GPG agent",
        src="assets/arch/gpg-agent.conf",
        dest=chroot_prefix(f"/home/{host.data.user}/.gnupg/gpg-agent.conf"),
    )
    files.file(
        name="Configure GPG agent permissions",
        path=f"/home/{host.data.user}/.gnupg/gpg-agent.conf",
        user=host.data.user,
        group=host.data.user,
        mode="0600",
        _shell_executable=chroot_shell(),
    )
    files.template(
        name="Configure user environment",
        src="assets/arch/.pam_environment.j2",
        dest=chroot_prefix(f"/home/{host.data.user}/.pam_environment"),
        pam_user=host.data.user,
    )
    files.file(
        name="Configure user environment permissions",
        path=f"/home/{host.data.user}/.pam_environment",
        user=host.data.user,
        group=host.data.user,
        _shell_executable=chroot_shell(),
    )


BASE_USER_PACKAGES = [
    "git",
    "zsh",
    "vim",
    "openssh",
    "gnupg",
    "opendoas",
]
