from typing import List

from pyinfra import host


def shell_now(command: str) -> List[str]:
    out = host.run_shell_command(command)
    return out[1]


def is_wsl() -> bool:
    distro = shell_now("echo $WSL_DISTRO_NAME")
    return isinstance(distro, str) and distro.strip() != ""


def chroot_shell() -> str:
    return "virtuoso-chroot" if not is_wsl() else "sh"


def chroot_prefix(path: str) -> str:
    return ("" if is_wsl() else "/mnt") + path
