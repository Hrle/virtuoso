from pyinfra.api import deploy
from virtuoso.arch.deploy.helpers import shell_now


@deploy("Setup shells")
def setup_chroot_shell():
    shell_now(
        """\
cat << EOF >/usr/bin/virtuoso-chroot
#!/usr/bin/env sh

set -eo pipefail

if [ ! -z "\\$WSL_DISTRO_NAME" ]; then
  sh "\\$@"
  exit
fi
  
if [ -x '/mnt/usr/bin/env' ]; then
  arch-chroot /mnt /usr/bin/env sh "\\$@"
  exit
fi

sh "\\$@"

EOF

chmod +x /usr/bin/virtuoso-chroot

"""
    )
