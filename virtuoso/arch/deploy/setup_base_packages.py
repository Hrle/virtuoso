from pyinfra.api import deploy
from pyinfra.operations import files, server
from pyinfra import host, logger
from virtuoso.arch.deploy.helpers import is_wsl, chroot_prefix, chroot_shell
from virtuoso.arch.deploy.setup_chroot_shell import setup_chroot_shell


@deploy("Setup base packages", data_defaults={"user": "user"})
def setup_base_packages():
    if is_wsl():
        logger.info("Skipping base package install on WSL")
        return
    else:
        setup_chroot_shell()

    server.shell(
        name="Install base packages",
        commands=f"\
          paru -S --noconfirm --disable-download-timeout \
            {' '.join(PACKAGES)}",
        _shell_executable=chroot_shell(),
    )
    files.link(
        name="Emoji config",
        path="/usr/share/fontconfig/conf.avail/75-twemoji.conf",
        target="/etc/fonts/conf.d/75-twemoji.conf",
        _shell_executable=chroot_shell(),
    )
    files.put(
        name="Boot backup hook",
        src="assets/arch/boot-backup.hook",
        dest=chroot_prefix("/etc/pacman.d/hooks/boot-backup.hook"),
    )
    files.put(
        name="Journald config",
        src="assets/arch/max-use.conf",
        dest=chroot_prefix("/etc/systemd/journald.conf.d/max-use.conf"),
    )
    files.put(
        name="Touchpad config",
        src="assets/arch/30-touchpad.conf",
        dest=chroot_prefix("/etc/xorg.conf.d/30-touchpad.conf"),
    )
    files.put(
        name="Root snapper config",
        src="assets/arch/root",
        dest=chroot_prefix("/etc/snapper/configs/root"),
    )
    files.template(
        name="User snapper config",
        src="assets/arch/user.j2",
        dest=chroot_prefix("/etc/snapper/configs/user"),
        snapper_user=host.data.user,
    )
    files.put(
        name="Snapper config",
        src="assets/arch/snapper",
        dest=chroot_prefix("/etc/conf.d/snapper"),
    )

    server.shell(
        name="Enable realtime privileges",
        commands=f'usermod -aG realtime "{host.data.user}"',
        _shell_executable=chroot_shell(),
    )
    server.shell(
        name="Enable base services",
        commands=[
            "systemctl enable reflector.timer",
            "systemctl enable btrfs-balance.timer",
            "systemctl enable btrfs-defrag.timer",
            "systemctl enable btrfs-scrub.timer",
            "systemctl enable btrfs-trim.timer",
            "systemctl enable fstrim.timer",
            "systemctl enable snapper-boot.timer",
            "systemctl enable snapper-cleanup.timer",
            "systemctl enable snapper-timeline.timer",
            "systemctl enable ananicy.service",
            "systemctl enable earlyoom.service",
            "systemctl disable sddm.service",
            "systemctl enable sddm-plymouth.service",
            "systemctl enable NetworkManager",
            "systemctl enable ufw",
            "ufw enable",
        ],
        _shell_executable=chroot_shell(),
    )


PACKAGES = [
    "rsync",
    "which",
    "python",
    "python-pipenv",
    "arch-install-scripts",
    "reflector",
    "namcap",
    "snap-pac",
    "snap-pac-grub",
    "snapper",
    "btrfsmaintenance",
    "ananicy",
    "earlyoom",
    "opendoas",
    "git",
    "openssh",
    "gnupg",
    "gvim",
    "man-db",
    "man-pages",
    "texinfo",
    "zsh",
    "fish",
    "nushell",
    "git-delta",
    "ranger",
    "networkmanager",
    "openvpn",
    "gufw",
    "ppp",
    "wireless_tools",
    "iw",
    "wpa_supplicant",
    "network-manager-applet",
    "network-manager-sstp",
    "networkmanager-fortisslvpn",
    "networkmanager-openvpn",
    "pipewire",
    "pipewire-alsa",
    "pipewire-jack",
    "pipewire-pulse",
    "alsa-utils",
    "alsa-plugins",
    "realtime-privileges",
    "sof-firmware",
    "gst-plugin-pipewire",
    "pipewire-v4l2",
    "pipewire-zeroconf",
    "wireplumber",
    "pasystray",
    "playerctl",
    "pavucontrol",
    "bluez-libs",
    "libinput",
    "libinput-gestures",
    "brightnessctl",
    "acpi",
    "fprintd",
    "xorg-server",
    "xorg-xauth",
    "xorg-xwininfo",
    "xorg-xvinfo",
    "xorg-xrandr",
    "xorg-xkill",
    "xorg-xinput",
    "xorg-xgamma",
    "xorg-xdpyinfo",
    "xorg-setxkbmap",
    "xorg-util-macros",
    "numlockx",
    "xclip",
    "gvfs",
    "gvfs-nfs",
    "gvfs-mtp",
    "gvfs-gphoto2",
    "lxsession-gtk3",
    "picom",
    "libglvnd",
    "xorg-mkfontscale",
    "xorg-font-util",
    "xorg-fonts-100dpi",
    "gnu-free-fonts",
    "opendesktop-fonts",
    "noto-fonts",
    "noto-fonts-emoji-flag-git",
    "noto-color-emoji-fontconfig",
    "ttf-twemoji",
    "noto-fonts-extra",
    "noto-fonts-cjk",
    "nerd-fonts-jetbrains-mono",
    "ttf-dejavu",
    "ttf-liberation",
    "wqy-zenhei",
    "sddm",
    "betterlockscreen",
    "python-dbus-next",
    "python-iwlib",
    "python-keyring",
    "python-psutil",
    "python-setproctitle",
    "python-pyxdg",
    "khal",
    "qtile",
    "mypy",
    "dunst",
    "rofi",
    "flameshot",
    "redshift",
    "gnome-keyring",
    "kitty",
    "starship",
    "xarchiver",
    "pcmanfm-gtk3",
    "lxtask",
    "lxinput-gtk3",
    "lxrandr-gtk3",
    "pamac-aur",
    "plasma-framework",
    "gtk-engine-murrine",
    "kvantum",
    "lxappearance-gtk3",
    "beautyline",
    "kvantum-theme-sweet-git",
    "sweet-gtk-theme-nova-git",
]
