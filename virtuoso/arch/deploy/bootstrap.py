from pyinfra.api import deploy
from pyinfra.operations import server
from pyinfra import host, logger
from virtuoso.arch.deploy.helpers import shell_now, is_wsl

# detect other stuff on device and fill in the blanks


@deploy(
    "Bootstrap",
    data_defaults={
        "machine": "virtuoso",
        "device": "/dev/sda",
        "user": "virtuoso",
    },
)
def bootstrap():
    if is_wsl():
        logger.info("Skipping bootstrap on WSL")
        return

    did_bootstrap = shell_now("cat /mnt/etc/hostname")
    if (
        did_bootstrap
        and len(did_bootstrap) > 0
        and did_bootstrap[0] == host.data.machine
    ):
        logger.info("Bootstrap completed earlier")
        return
    else:
        logger.info("Bootstrapping")

    mount = shell_now("mount | grep /mnt")
    if mount and len(mount) > 0 and mount[0]:
        shell_now("swapoff -a")
        shell_now("umount -R /mnt")

    server.shell(name="Set time", commands="hwclock --systohc")

    boot = 1
    root = boot + 1
    subvolumes = [
        "/.swap",
        "/.snapshots",
        "/var/log",
        f"/home/{host.data.user}",
        f"/home/{host.data.user}/.snapshots",
    ]

    if "nvme" in host.data.device:

        def partition(num):
            return f"{host.data.device}p{num}"

    else:

        def partition(num):
            return f"{host.data.device}{num}"

    server.shell(
        name="Create partitions",
        commands=[
            f"parted --script {host.data.device} {command}"
            for command in [
                "mktable gpt",
                "mkpart arch-boot fat32 0% 4GB",
                f"toggle {boot} esp",
                "mkpart root btrfs 4GB 100%",
            ]
        ],
    )

    server.shell(
        name="Format partitions",
        commands=[
            f"mkfs.fat -F 32 {partition(boot)}",
            f"mkfs.btrfs -f {partition(root)}",
        ],
    )

    server.shell(
        name="Mount partitions",
        commands=[
            f"mount -o {BTRFS_PARAMETERS} {partition(root)} /mnt",
            "mkdir /mnt/boot",
            f"mount {partition(boot)} /mnt/boot",
        ],
    )

    server.shell(
        name="Create subvolumes",
        commands=[
            "mkdir /mnt/var",
            "mkdir /mnt/home",
            *[
                f"btrfs subvolume create /mnt{subvolume}"
                for subvolume in subvolumes
            ],
        ],
    )

    server.shell(
        name="Mount subvolumes",
        commands=[
            *[
                f"mount -o {BTRFS_PARAMETERS},subvol={path} {partition(root)} /mnt{path}"
                for path in subvolumes
            ],
        ],
    )

    server.shell(
        name="Create swapfile",
        commands=[
            "btrfs filesystem mkswapfile /mnt/.swap/swapfile",
            "swapon /mnt/.swap/swapfile",
        ],
    )

    server.shell(
        name="Install base system",
        commands=[
            "pacstrap /mnt base",
            "genfstab -U /mnt /mnt/etc/fstab",
            f'printf "{host.data.machine}\\n" > "/mnt/etc/hostname"',
        ],
    )


BTRFS_PARAMETERS = (
    "rw,noatime,compress=zstd:3,ssd,space_cache=v2,autodefrag,discard=async"
)
