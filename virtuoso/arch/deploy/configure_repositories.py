from pyinfra.api import deploy
from pyinfra.operations import files, server
from pyinfra import logger
from virtuoso.arch.deploy.setup_chroot_shell import setup_chroot_shell
from virtuoso.arch.deploy.helpers import (
    is_wsl,
    chroot_shell,
    chroot_prefix,
    shell_now,
)


@deploy("Configure repositories")
def configure_repositories():
    if not is_wsl():
        setup_chroot_shell()

    paru = shell_now(f"ls {chroot_prefix('/usr/bin/paru')}")
    if paru and len(paru) > 0 and paru[0]:
        logger.info("Repositories configured earlier")
        return
    else:
        logger.info("Configuring repositories")

    pacman_ignored_packages = (
        WSL_PACMAN_IGNORED_PACKAGES if is_wsl() else PC_PACMAN_IGNORED_PACKAGES
    )

    server.shell(
        name="Install base-devel",
        commands=f"pacman -S \
          --noconfirm \
          --disable-download-timeout \
          --ignore {' --ignore '.join(pacman_ignored_packages)} \
          base-devel",
        _shell_executable=chroot_shell(),
    )

    server.shell(
        name="Setup Chaotic AUR",
        commands=[
            "pacman-key --recv-key FBA220DFC880C036 \
              --keyserver keyserver.ubuntu.com",
            "pacman-key --lsign-key FBA220DFC880C036",
            f"pacman -U --noconfirm --disable-download-timeout \
              {' '.join(CHAOTIC_PACKAGES)} ",
        ],
        _shell_executable=chroot_shell(),
    )

    server.shell(
        name="Setup Black Arch",
        commands="curl -sL https://blackarch.org/strap.sh | bash",
        _shell_executable=chroot_shell(),
    )

    files.template(
        name="Configure pacman",
        src="assets/arch/pacman.conf.j2",
        dest=chroot_prefix("/etc/pacman.conf"),
        pacman_ignored_packages=" ".join(pacman_ignored_packages),
    )

    server.shell(
        name="Setup paru",
        commands=[
            "pacman -Syyu --noconfirm --disable-download-timeout",
            "pacman -S --noconfirm --disable-download-timeout paru",
            "paru -Syyu --noconfirm --disable-download-timeout",
        ],
        _shell_executable=chroot_shell(),
    )


WSL_PACMAN_IGNORED_PACKAGES = ["fakeroot"]

PC_PACMAN_IGNORED_PACKAGES = ["ttf-google-fonts-git"]

CHAOTIC_PACKAGES = [
    "'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'",
    "'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'",
]
