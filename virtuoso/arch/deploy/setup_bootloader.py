from pyinfra.api import deploy
from pyinfra.operations import files, server
from pyinfra import logger
from virtuoso.arch.deploy.setup_chroot_shell import setup_chroot_shell
from virtuoso.arch.deploy.helpers import shell_now, is_wsl


@deploy("Setup the bootloader")
def setup_bootloader():
    if is_wsl():
        logger.info("Skipping bootloader setup on WSL")
        return

    setup_chroot_shell()

    grubcfg = shell_now("ls /mnt/boot/grub/grub.cfg")
    if grubcfg and len(grubcfg) > 0 and grubcfg[0]:
        logger.info("Bootloader setup earlier")
        return
    else:
        logger.info("Running bootloader setup")

    cpu_info = shell_now(
        "grep vendor_id /proc/cpuinfo | head -n 1 | awk '{ print $3 }'"
    )
    cpu = None
    if not cpu_info or len(cpu_info) == 0 or not cpu_info[0]:
        cpu_info = [""]
    if "AuthenticAMD" in cpu_info[0]:
        cpu = "amd"
    if "GenuineIntel" in cpu_info[0]:
        cpu = "intel"
    logger.info(f"Detected {'no' if cpu is None else cpu} CPU")

    gpu_info = shell_now("lspci | grep VGA")
    gpu = None
    gpu_drivers = []
    gpu_modules = []
    gpu_parameters = []
    if not gpu_info or len(gpu_info) == 0 or not gpu_info[0]:
        gpu_info = [""]
    if "NVIDIA" in gpu_info[0]:
        gpu = "NVIDIA"
        gpu_drivers = NVIDIA_DRIVERS
        gpu_modules = NVIDIA_MODULES
        gpu_parameters = NVIDIA_PARAMETERS
    if "AMD" in gpu_info[0]:
        gpu = "AMD"
        gpu_drivers = AMD_DRIVERS
        gpu_modules = AMD_MODULES
    if "Intel" in gpu_info[0]:
        gpu = "intel"
        gpu_drivers = INTEL_DRIVERS
        gpu_modules = INTEL_MODULES
    if "Virtio" in gpu_info[0]:
        gpu = "virtio"
        gpu_modules = VIRTIO_MODULES
    logger.info(f"Detected {'no' if gpu is None else gpu} GPU")

    resume = shell_now("lsblk --fs | grep swap | awk '{ print $4 }'")
    logger.info(f"Detected {resume} SWAP")

    server.shell(
        name="Install bootloader packages",
        commands="paru -Syy --noconfirm --disable-download-timeout "
        + " ".join(
            [
                *GENERIC_PACKAGES,
                *([] if cpu is None else [f"{cpu}-ucode"]),
                *gpu_drivers,
            ]
        ),
        _shell_executable="virtuoso-chroot",
    )

    files.template(
        name="Configure mkinitcpio",
        src="assets/arch/mkinitcpio.conf.j2",
        dest="/mnt/etc/mkinitcpio.conf",
        mkinitcpio_modules=" ".join(
            [*GENERIC_MKINITCPIO_MODULES, *gpu_modules]
        ),
        mkinitcpio_hooks=" ".join([*GENERIC_MKINITCPIO_HOOKS]),
    )
    files.template(
        name="Configure GRUB",
        src="assets/arch/grub.j2",
        dest="/mnt/etc/default/grub",
        kernel_parameters=" ".join(
            [
                *GENERIC_KERNEL_PARAMETERS,
                *gpu_parameters,
                # figure out later
                # f"resume={resume[0]}",
            ]
        ),
    )
    server.shell(
        name="Install bootloader",
        commands=[
            "mkinitcpio -P",
            "os-prober",
            f'\
              grub-install \
                --target=x86_64-efi \
                --efi-directory=/boot \
                --bootloader-id=GRUB \
                --modules="{" ".join([*GENERIC_GRUB_MODULES])}" \
                --sbat /usr/share/grub/sbat.csv \
                --disable-shim-lock',
        ],
        _shell_executable="virtuoso-chroot",
    )

    server.shell(
        name="Enable secure boot if available",
        commands="\
            sbctl status | grep 'Secure Boot' | grep -q 'Enabled' && \
            sbctl status | grep 'Setup Mode' | grep -q 'Enabled' && \
            echo 'Secure boot available' && \
            sbctl create-keys && \
            chattr -i /sys/firmware/efi/efivars/PK-* && \
            chattr -i /sys/firmware/efi/efivars/KEK-* && \
            chattr -i /sys/firmware/efi/efivars/db-* && \
            sbctl enroll-keys -m && \
            sbctl sign -s /boot/efi/grub/grubx64.efi && \
            find /boot -maxdepth 1 -name 'vmlinuz-*' | \
               xargs -IR sbctl sign -s R || \
            echo 'Secure boot not available'",
        _shell_executable="virtuoso-chroot",
    )

    server.shell(
        name="Configure GRUB",
        commands="grub-mkconfig -o /boot/grub/grub.cfg",
        _shell_executable="virtuoso-chroot",
    )


GENERIC_PACKAGES = [
    "linux-zen",
    "linux-zen-headers",
    "linux-lts",
    "linux-lts-headers",
    "linux-rt",
    "linux-rt-headers",
    "linux-firmware",
    "mkinitcpio-firmware",
    "libxkbcommon",
    "plymouth-git",
    "xf86-input-libinput",
    "efibootmgr",
    "btrfs-progs",
    "dosfstools",
    "mtools",
    "grub",
    "grub-btrfs",
    "sbctl",
    "os-prober",
]

NVIDIA_DRIVERS = [
    "nvidia-dkms",
    "nvidia-utils",
    "lib32-nvidia-utils",
    "vulkan-icd-loader",
    "lib32-vulkan-icd-loader",
    "nvidia-settings",
    "vulkan-tools",
]

NVIDIA_MODULES = ["nvidia", "nvidia_modeset", "nvidia_uvm", "nvidia_drm"]

NVIDIA_PARAMETERS = ["nvidia-drm.modeset=1"]

AMD_DRIVERS = [
    "xf86-video-amdgpu",
    "mesa",
    "lib32-mesa",
    "vulkan-radeon",
    "lib32-vulkan-radeon",
    "vulkan-icd-loader",
    "lib32-vulkan-icd-loader",
    "libva-mesa-driver",
    "mesa-vdpau",
    "vulkan-tools",
]

AMD_MODULES = ["amdgpu", "radeon"]

INTEL_DRIVERS = [
    "xf86-video-intel",
    "mesa",
    "lib32-mesa",
    "vulkan-intel",
    "lib32-vulkan-intel",
    "vulkan-icd-loader",
    "lib32-vulkan-icd-loader",
    "intel-media-driver",
    "vulkan-tools",
]

INTEL_MODULES = ["i915"]

VIRTIO_MODULES = ["virtio-gpu"]

GENERIC_MKINITCPIO_MODULES = ["btrfs"]

GENERIC_MKINITCPIO_HOOKS = [
    "base",
    "udev",
    "plymouth",
    "autodetect",
    "keyboard",
    "modconf",
    "block",
    "filesystems",
    "resume",
]

GENERIC_KERNEL_PARAMETERS = [
    "quiet",
    "splash",
    "vt.global_cursor_default=0",
    "vga=current",
    "loglevel=3",
    "rd.systemd.show_status=auto",
    "rd.udev.log_level=3",
]

GENERIC_GRUB_MODULES = [
    "all_video",
    "boot",
    "btrfs",
    "cat",
    "chain",
    "configfile",
    "echo",
    "efifwsetup",
    "efinet",
    "ext2",
    "fat",
    "font",
    "gettext",
    "gfxmenu",
    "gfxterm",
    "gfxterm_background",
    "gzio",
    "halt",
    "help",
    "hfsplus",
    "iso9660",
    "jpeg",
    "keystatus",
    "loadenv",
    "loopback",
    "linux",
    "ls",
    "lsefi",
    "lsefimmap",
    "lsefisystab",
    "lssal",
    "memdisk",
    "minicmd",
    "normal",
    "ntfs",
    "part_apple",
    "part_msdos",
    "part_gpt",
    "password_pbkdf2",
    "png",
    "probe",
    "reboot",
    "regexp",
    "search",
    "search_fs_uuid",
    "search_fs_file",
    "search_label",
    "sleep",
    "smbios",
    "squash4",
    "test",
    "true",
    "video",
    "xfs",
    "zfs",
    "zfscrypt",
    "zfsinfo",
    "cpuid",
    "play",
    "tpm",
    "cryptodisk",
    "gcry_arcfour",
    "gcry_blowfish",
    "gcry_camellia",
    "gcry_cast5",
    "gcry_crc",
    "gcry_des",
    "gcry_dsa",
    "gcry_idea",
    "gcry_md4",
    "gcry_md5",
    "gcry_rfc2268",
    "gcry_rijndael",
    "gcry_rmd160",
    "gcry_rsa",
    "gcry_seed",
    "gcry_serpent",
    "gcry_sha1",
    "gcry_sha256",
    "gcry_sha512",
    "gcry_tiger",
    "gcry_twofish",
    "gcry_whirlpool",
    "luks",
    "lvm",
    "mdraid09",
    "mdraid1x",
    "raid5rec",
    "raid6rec",
]
