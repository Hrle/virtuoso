from pyinfra.api import deploy
from pyinfra.operations import files, server
from pyinfra import logger
from virtuoso.arch.deploy.helpers import is_wsl, chroot_prefix, chroot_shell


@deploy("Configure themes")
def configure_themes():
    if is_wsl():
        logger.info("Skipping theme configuration on WSL")
        return

    files.download(
        name="Download GRUB theme",
        src="https://gitlab.com/hrle/ultraviolet-grub-theme/-/archive/main/ultraviolet-grub-theme.tar.gz",
        dest="/tmp/ultraviolet-grub-theme.tar.gz",
        _shell_executable=chroot_shell(),
    )
    files.directory(
        name="Create GRUB theme directory",
        path="/usr/share/grub/themes/ultraviolet",
        _shell_executable=chroot_shell(),
    )
    server.shell(
        name="Extract GRUB theme",
        commands="\
          tar -xzf /tmp/ultraviolet-grub-theme.tar.gz \
            -C /usr/share/grub/themes/ultraviolet \
            --strip-components=1",
        _shell_executable=chroot_shell(),
    )
    files.line(
        path=chroot_prefix("/etc/default/grub"),
        line="GRUB_THEME=ultraviolet",
    )
    server.shell(
        name="Generate GRUB config",
        commands="grub-mkconfig -o /boot/grub/grub.cfg",
        _shell_executable=chroot_shell(),
    )
    files.file(
        name="Remove GRUB theme tarball",
        path="/tmp/ultraviolet-grub-theme.tar.gz",
        present=False,
        _shell_executable=chroot_shell(),
    )

    files.download(
        name="Download Plymouth theme",
        src="https://gitlab.com/hrle/plymouth-sweet-arch-theme/-/archive/main/plymouth-sweet-arch-theme.tar.gz",
        dest="/tmp/plymouth-sweet-arch-theme.tar.gz",
        _shell_executable=chroot_shell(),
    )
    files.directory(
        name="Create Plymouth theme directory",
        path="/usr/share/plymouth/themes/sweet-arch",
        _shell_executable=chroot_shell(),
    )
    server.shell(
        name="Extract Plymouth theme",
        commands="\
          tar -xzf /tmp/plymouth-sweet-arch-theme.tar.gz \
            -C /usr/share/plymouth/themes/sweet-arch \
            --strip-components=1",
        _shell_executable=chroot_shell(),
    )
    files.put(
        name="Configure Plymouth",
        src="assets/arch/plymouthd.conf",
        dest=chroot_prefix("/etc/plymouth/plymouthd.conf"),
    )
    files.file(
        name="Remove Plymouth theme tarball",
        path="/tmp/plymouth-sweet-arch-theme.tar.gz",
        present=False,
        _shell_executable=chroot_shell(),
    )

    files.download(
        name="Download SDDM theme",
        src="https://gitlab.com/hrle/sddm-sweet-theme/-/archive/main/sddm-sweet-theme.tar.gz",
        dest="/tmp/sddm-sweet-theme.tar.gz",
        _shell_executable=chroot_shell(),
    )
    files.directory(
        name="Create SDDM theme directory",
        path="/usr/share/sddm/themes/sweet",
        _shell_executable=chroot_shell(),
    )
    server.shell(
        name="Extract SDDM theme",
        commands="\
          tar -xzf /tmp/sddm-sweet-theme.tar.gz \
            -C /usr/share/sddm/themes/sweet \
            --strip-components=1",
        _shell_executable=chroot_shell(),
    )
    files.put(
        name="Configure SDDM",
        src="assets/arch/theme.conf",
        dest=chroot_prefix("/etc/sddm.conf.d/theme.conf"),
    )
    files.file(
        name="Remove SDDM theme tarball",
        path="/tmp/sddm-sweet-theme.tar.gz",
        present=False,
        _shell_executable=chroot_shell(),
    )
