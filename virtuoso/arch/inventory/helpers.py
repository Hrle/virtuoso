from getpass import getpass


def retype(prompt: str, default: str = "") -> str:
    capitalized = prompt[0].upper() + prompt[1:]
    lowercased = prompt[0].lower() + prompt[1:]

    while True:
        password = getpass(f"{capitalized}: ")
        if default and not password:
            return default

        retyped_password = getpass(f"Retype {lowercased}: ")

        if password == retyped_password:
            return password
        else:
            print(f"{capitalized} does not match. Please try again.")
