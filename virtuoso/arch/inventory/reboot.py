from virtuoso.arch.inventory.helpers import retype

print(
    """\
This is an Arch linux install inventory file. It is used to define the machine
on which Arch linux will be installed. It is also used to define the user that
will be created on the machine. Please take your time to SSH into the machine
beforehand and confirm the values you want.
"""
)

machine = retype("New machine name")

machines = [
    (
        f"@ssh/{machine}",
        {
            "zoneinfo": retype("Zoneinfo", "Europe/Zagreb"),
            "machine": machine,
            "device": retype("Device to install Arch Linux on", "/dev/sda"),
            "user": retype("Machine user", "virtuoso"),
        },
    )
]
