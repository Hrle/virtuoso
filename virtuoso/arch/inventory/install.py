from virtuoso.arch.inventory.helpers import retype

print(
    """\
This is an Arch linux install inventory file. It is used to define the machine
on which Arch linux will be installed. It is also used to define the user that
will be created on the machine. Please take your time to SSH into the machine
beforehand and confirm the values you want.
"""
)

machine = [
    (
        "@ssh/archiso",
        {
            "machine": retype("New machine name", "virtuoso"),
            "device": retype("Device to install on", "/dev/sda"),
            "user": retype("New machine user name", "virtuoso"),
            "password": retype("Password for the new machine", "virtuoso"),
            "dotfiles": retype(
                "Dotfiles repository", "https://gitlab.com/hrle/dotfiles-arch"
            ),
        },
    )
]
