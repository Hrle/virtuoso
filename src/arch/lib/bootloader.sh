#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Setting up the bootloader...\n"

MACHINE=$1
if ! echo "$MACHINE" | grep -Eq "[A-Z]+"; then
  printf "[VIRTUOSO]: Please enter a valid machine name for the 1st argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s machine\n" "$MACHINE"

if [ ! "$(which paru)" ]; then
  printf "[VIRTUOSO]: Please install paru first\n"
  exit 1
fi

case $(
  grep vendor_id /proc/cpuinfo |
    head -n 1 |
    awk '{ print $3 }'
) in
AuthenticAMD)
  CPU="amd"
  ;;
GenuineIntel)
  CPU="intel"
  ;;
*)
  printf "[VIRTUOSO]: Unrecognized CPU\n"
  exit 1
  ;;
esac
printf "[VIRTUOSO]: Detected %s CPU\n" "$CPU"

case $(lspci | grep VGA) in
*NVIDIA*)
  GPU="NVIDIA"
  GPU_DRIVERS="\
nvidia-dkms \
nvidia-utils \
lib32-nvidia-utils \
vulkan-icd-loader \
lib32-vulkan-icd-loader \
nvidia-settings \
vulkan-tools \
"
  GPU_MODULES="\
nvidia \
nvidia_modeset \
nvidia_uvm \
nvidia_drm \
"
  GPU_PARAMETERS="\
nvidia-drm.modeset=1 \
"
  ;;
*AMD*)
  GPU="AMD"
  GPU_DRIVERS="\
xf86-video-amdgpu \
mesa \
lib32-mesa \
vulkan-radeon \
lib32-vulkan-radeon \
vulkan-icd-loader \
lib32-vulkan-icd-loader \
libva-mesa-driver \
mesa-vdpau \
vulkan-tools \
"
  GPU_MODULES="\
amdgpu \
radeon \
"
  ;;
*Intel*)
  GPU="Intel"
  GPU_DRIVERS="\
xf86-video-intel \
mesa \
lib32-mesa \
vulkan-intel \
lib32-vulkan-intel \
vulkan-icd-loader \
lib32-vulkan-icd-loader \
intel-media-driver \
vulkan-tools \
"
  GPU_MODULES="\
i915 \
"
  ;;
*Virtio*)
  GPU="Virtio"
  GPU_DRIVERS=""
  GPU_MODULES="\
virtio-gpu \
"
  ;;
*)
  printf "[VIRTUOSO]: Unrecognized GPU"
  exit 1
  ;;
esac
printf "[VIRTUOSO]: Detected %s GPU\n" "$GPU"
printf "[VIRTUOSO]: Using %s GPU drivers\n" "$GPU_DRIVERS"
printf "[VIRTUOSO]: Using %s GPU modules\n" "$GPU_MODULES"

#shellcheck disable=SC2086
if ! paru -S --noconfirm --noprogressbar --disable-download-timeout \
  linux-zen \
  linux-zen-headers \
  linux-lts \
  linux-lts-headers \
  linux-rt \
  linux-rt-headers \
  linux-firmware \
  mkinitcpio-firmware \
  plymouth-git \
  ${CPU}-ucode \
  $GPU_DRIVERS \
  xf86-input-libinput \
  efibootmgr \
  btrfs-progs \
  dosfstools \
  mtools \
  grub \
  grub-btrfs \
  sbctl \
  os-prober; then
  printf "\n[VIRTUOSO]: Failed installing bootloader packages\n"
  exit 1
fi

printf "%s" "$MACHINE" >/etc/hostname
printf "[VIRTUOSO]: Set %s machine\n" "$MACHINE"
cat /etc/hostname

MODULES="\
btrfs \
$GPU_MODULES \
"
printf "[VIRTUOSO]: Using %s modules\n" "$MODULES"

HOOKS="\
base \
udev \
plymouth \
autodetect \
keyboard \
modconf \
block \
filesystems \
resume \
"
printf "[VIRTUOSO]: Using %s hooks\n" "$HOOKS"

# TODO: switch to swapfile
RESUME="UUID=$(lsblk --fs | grep swap | awk '{ print $4 }')"

PARAMETERS="\
$GPU_PARAMETERS \
quiet \
splash \
vt.global_cursor_default=0 \
vga=current \
loglevel=3 \
rd.systemd.show_status=auto \
rd.udev.log_level=3 \
resume=$RESUME \
"
printf "[VIRTUOSO]: Using %s parameters\n" "$PARAMETERS"

GRUB_MODULES="\
all_video \
boot \
btrfs \
cat \
chain \
configfile \
echo \
efifwsetup \
efinet \
ext2 \
fat \
font \
gettext \
gfxmenu \
gfxterm \
gfxterm_background \
gzio \
halt \
help \
hfsplus \
iso9660 \
jpeg \
keystatus \
loadenv \
loopback \
linux \
ls \
lsefi \
lsefimmap \
lsefisystab \
lssal \
memdisk \
minicmd \
normal \
ntfs \
part_apple \
part_msdos \
part_gpt \
password_pbkdf2 \
png \
probe \
reboot \
regexp \
search \
search_fs_uuid \
search_fs_file \
search_label \
sleep \
smbios \
squash4 \
test \
true \
video \
xfs \
zfs \
zfscrypt \
zfsinfo \
cpuid \
play \
tpm \
cryptodisk \
gcry_arcfour \
gcry_blowfish \
gcry_camellia \
gcry_cast5 \
gcry_crc \
gcry_des \
gcry_dsa \
gcry_idea \
gcry_md4 \
gcry_md5 \
gcry_rfc2268 \
gcry_rijndael \
gcry_rmd160 \
gcry_rsa \
gcry_seed \
gcry_serpent \
gcry_sha1 \
gcry_sha256 \
gcry_sha512 \
gcry_tiger \
gcry_twofish \
gcry_whirlpool \
luks \
lvm \
mdraid09 \
mdraid1x \
raid5rec \
raid6rec \
"
printf "[VIRTUOSO]: Using GRUB modules: %s \n" "$GRUB_MODULES"

cat <<END >/etc/mkinitcpio.conf
MODULES=($MODULES)
BINARIES=()
FILES=()
HOOKS=($HOOKS)
END
printf "[VIRTUOSO]: Configured initramfs\n"
cat /etc/mkinitcpio.conf

cat <<END >/etc/default/grub
GRUB_DISTRIBUTOR="Arch"

GRUB_DEFAULT=0
GRUB_TIMEOUT=5
GRUB_TIMEOUT_STYLE=menu

GRUB_PRELOAD_MODULES=""
GRUB_CMDLINE_LINUX_DEFAULT="$PARAMETERS"
GRUB_CMDLINE_LINUX=""
GRUB_DISABLE_OS_PROBER=false

GRUB_TERMINAL_INPUT=console
END
printf "[VIRTUOSO]: Configured bootloader\n"
cat /etc/default/grub

mkinitcpio -P
os-prober
grub-install \
  --target=x86_64-efi \
  --efi-directory=/boot \
  --bootloader-id=GRUB \
  --modules="$GRUB_MODULES" \
  --sbat /usr/share/grub/sbat.csv \
  --disable-shim-lock
grub-mkconfig -o /boot/grub/grub.cfg
printf "[VIRTUOSO]: Installed bootloader\n"

sbctl create-keys
chattr -i /sys/firmware/efi/efivars/PK-*
chattr -i /sys/firmware/efi/efivars/KEK-*
chattr -i /sys/firmware/efi/efivars/db-*
sbctl enroll-keys -m
sbctl sign -s /boot/efi/grub/grubx64.efi
find /boot \
  -maxdepth 1 \
  -name 'vmlinuz-*' \
  -exec /usr/bin/sh -c 'sbctl sign -s "{}"' \;
printf "[VIRTUOSO]: Created and enrolled keys\n"
sbctl status
sbctl verify

printf "\n[VIRTUOSO]: The bootloader was set up\n"
