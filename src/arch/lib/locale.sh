#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Configuring locale...\n"

ZONEINFO=$1
if ! echo "$ZONEINFO" | grep -Eq "[A-Z][a-z]+\/[A-Z][a-z]+"; then
  printf "[VIRTUOSO]: Please enter a valid zoneinfo for the 1st argument"
  exit 1
fi
printf "[VIRTUOSO]: Using %s zoneinfo\n" "$ZONEINFO"

hwclock --systohc
ln -sf "/usr/share/zoneinfo/$ZONEINFO" /etc/localtime
printf 'en_US.UTF-8 UTF-8\n' >/etc/locale.gen
printf 'LANG=en_US.UTF-8\n' >/etc/locale.conf
locale-gen

printf "\n[VIRTUOSO]: Configured locale\n"
