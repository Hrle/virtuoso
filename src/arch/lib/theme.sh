#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Configuring themes...\n"

if ! which git; then
  if ! pacman -S --noconfirm --noprogressbar --disable-download-timeout git; then
    printf "[VIRTUOSO]: Failed installing git\n"
    exit 1
  fi
fi

if git clone --depth 1 https://gitlab.com/Hrle/ultraviolet-grub-theme; then
  mkdir --parents /usr/share/grub/themes
  mv ultraviolet-grub-theme/ultraviolet /usr/share/grub/themes
  rm -rf ultraviolet-grub-theme
  printf "GRUB_THEME=\"/usr/share/grub/themes/ultraviolet/theme.txt\"\n" >>/etc/default/grub
  which grub-mkconfig && grub-mkconfig -o /boot/grub/grub.cfg
else
  printf "[VIRTUOSO]: Failed pulling GRUB theme\n"
fi

if git clone --depth 1 https://gitlab.com/Hrle/plymouth-sweet-arch-theme; then
  mkdir --parents /usr/share/plymouth/themes
  mv plymouth-sweet-arch-theme/sweet-arch /usr/share/plymouth/themes
  rm -rf plymouth-sweet-arch-theme
  mkdir --parents /etc/plymouth
  printf "[Daemon]\nTheme=sweet-arch\nShowDelay=0\nDeviceTimeout=5\n" >/etc/plymouth/plymouthd.conf
else
  printf "[VIRTUOSO]: Failed pulling Plymouth theme\n"
fi

if git clone --depth 1 https://gitlab.com/Hrle/sddm-sweet-theme; then
  mkdir --parents /usr/share/sddm/themes
  mv sddm-sweet-theme/sweet /usr/share/sddm/themes
  rm -rf sddm-sweet-theme
  mkdir --parents /etc/sddm.conf.d
  printf "[Theme]\nCurrent=sweet\n" >/etc/sddm.conf.d/theme.conf
else
  printf "[VIRTUOSO]: Failed pulling SDDM theme\n"
fi

printf "\n[VIRTUOSO]: Configured themes\n"
