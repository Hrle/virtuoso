#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Configuring user...\n"

USER=$1
if ! echo "$USER" | grep -Eq "[A-Za-z0-9]+"; then
  printf "[VIRTUOSO]: Please enter a valid user name for the 1st argument"
  exit 1
fi
printf "[VIRTUOSO]: Using %s user\n" "$USER"

DOTFILES=$2
if ! echo "$DOTFILES" | grep -Eq "https.*"; then
  printf "[VIRTUOSO]: Please enter a valid dotfile repository URL for the 2nd argument"
  exit 1
fi
printf "[VIRTUOSO]: Using %s dotfiles\n" "$DOTFILES"

if ! which git; then
  if ! pacman -S --noconfirm --noprogressbar --disable-download-timeout git; then
    printf "[VIRTUOSO]: Failed installing git\n"
    exit 1
  fi
fi

if ! which zsh; then
  if ! pacman -S --noconfirm --noprogressbar --disable-download-timeout zsh; then
    printf "[VIRTUOSO]: Failed installing zsh\n"
    exit 1
  fi
fi

useradd "$USER"

usermod -aG wheel "$USER"

mkdir --parents "/home/$USER"
chown "$USER":"$USER" "/home/$USER"

sudo chsh -s /usr/bin/zsh "$USER"
mkdir "/home/$USER/.cache/zsh"

mkdir --parents "/opt/src/$USER/repos"
chown "$USER":"$USER" "/opt/src/$USER"
chown "$USER":"$USER" "/opt/src/$USER/repos"

printf "[VIRTUOSO]: Changing root password...\n"
until passwd; do
  printf "[VIRTUOSO]: Failed changing root password\n"
done

printf "[VIRTUOSO]: Changing password of user %s...\n" "$USER"
passwd --delete "$USER"
until passwd "$USER"; do
  printf "[VIRTUOSO]: Failed changing password of user %s\n" "$USER"
done

wd=$(pwd)
#shellcheck disable=SC2164
cd "/home/$USER"
su "$USER" -c "git init --initial-branch initial"
su "$USER" -c "git remote add readonly '$DOTFILES'"
if ! su "$USER" -c "git fetch readonly"; then
  printf "[VIRTUOSO]: Failed fetching dotfiles\n"
  exit 1
fi
if ! su "$USER" -c "git switch main"; then
  printf "[VIRTUOSO]: Failed switching to main on dotfiles\n"
  exit 1
fi
su "$USER" -c "git pull readonly main --recurse-submodules"
su "$USER" -c "git submodule update --init"
#shellcheck disable=SC2164
cd "$wd"

# TODO: password and hostname
# su "$USER" -c "ssh-keygen -a 100 -t ed25519 -C 'KARBURATOR' -f ~/.ssh/id_ed25519"

if [ -d ".gnupg" ]; then
  chmod 700 .gnupg
  chmod 600 .gnupg/gpg-agent.conf
fi

printf "REPOS DEFAULT=/opt/src/%s/repos\n" "$USER" >"/home/$USER/.pam_environment"

ls -la "/home/$USER"

printf "\n[VIRTUOSO]: Configured user\n"
