#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Configuring mirrors...\n"

cat <<END >/etc/resolv.conf
nameserver 1.1.1.1
nameserver 1.0.0.1
END

if ! pacman -Syy --noprogressbar --noconfirm --disable-download-timeout \
  pacman \
  arch-install-scripts \
  archlinux-keyring \
  pacman-mirrorlist \
  reflector; then
  printf "[VIRTUOSO]: Failed installing packages for mirror configuration\n"
  exit 1
fi

reflector --latest 10 --sort rate --save /etc/pacman.d/mirrorlist

cat /etc/pacman.d/mirrorlist

printf "\n[VIRTUOSO]: Configured mirrors\n"
