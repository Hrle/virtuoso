#!/usr/bin/env sh

printf "\n[VIRTUOSO]: Installing Arch Linux...\n"

ARCH_SCRIPTS="$(realpath "$(dirname "$0")")"

if [ ! "$(id -un)" = "root" ]; then
  printf "[VIRTUOSO]: Please run this as root\n"
  exit 1
fi

if [ ! "$VIRTUOSO_NOEXEC" ]; then
  if [ ! -d "/var/log/virtuoso" ]; then
    mkdir "/var/log/virtuoso"
  fi
  VIRTUOSO_NOEXEC=1 exec "$0" "$@" 2>&1 | tee "/var/log/virtuoso/install"
  exit
fi

ZONEINFO=$1
if ! echo "$ZONEINFO" | grep -Eq "[A-Z][a-z]+\/[A-Z][a-z]+"; then
  printf "[VIRTUOSO]: Please enter a valid zoneinfo for the 1st argument \n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s zoneinfo\n" "$ZONEINFO"

MACHINE=$2
if ! echo "$MACHINE" | grep -Eq "[A-Z]+"; then
  printf "[VIRTUOSO]: Please enter a valid machine name for the 2nd argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s machine\n" "$MACHINE"

USER=$3
if ! echo "$USER" | grep -Eq "[A-Za-z0-9]+"; then
  printf "[VIRTUOSO]: Please enter a valid user name for the 3rd argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s user\n" "$USER"

DEVICE=$4
if ! echo "$DEVICE" | grep -Eq "/dev/.*"; then
  printf "[VIRTUOSO]: Please enter a valid device for the 4th argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s device\n" "$DEVICE"

DOTFILES=$5
if ! echo "$DOTFILES" | grep -Eq "https.*"; then
  printf "[VIRTUOSO]: Please enter a valid dotfile repository URL for the 5th argument\n"
  exit 1
fi
printf "[VIRTUOSO]: Using %s dotfiles\n" "$DOTFILES"

"$ARCH_SCRIPTS/lib/mirrors.sh" || exit 1
"$ARCH_SCRIPTS/lib/device.sh" "$USER" "$DEVICE" || exit 1

printf "[VIRTUOSO]: Bootstrapping device...\n"
if ! pacstrap /mnt base which rsync; then
  printf "[VIRTUOSO]: Failed bootstrapping device\n"
  exit 1
fi
genfstab -U /mnt >/mnt/etc/fstab
cat /mnt/etc/fstab
printf "[VIRTUOSO]: Bootstrapped device\n"

cp -r "$ARCH_SCRIPTS" /mnt/opt/virtuoso
if ! arch-chroot /mnt /usr/bin/env sh -- \
  /opt/virtuoso/chroot.sh \
  "$ZONEINFO" \
  "$MACHINE" \
  "$USER" \
  "$DOTFILES" \
  ; then
  rm -rf /mnt/opt/virtuoso
  printf "\n[VIRTUOSO]: Failed installing Arch Linux\n"
  exit 1
else
  rm -rf /mnt/opt/virtuoso
fi

cat <<END
[VIRTUOSO]: Installed Arch Linux

Things to do after install:
 - check the logs
 - systemctl reboot
 - run the reboot.sh script
 - configure qtile hardware ids (/etc/profile.d/qtile.sh)
 - configure syncthing
 - configure keepassxc
 - configure radicale
 - configure evolution
 - configure ferdium
 - configure brave
 - configure ssh, gpg, fingerprint
 - configure firewall (transmission 51413, radicale 5232, aspnetcore 5000/5001)
 - betterlockscreen --update {wallpaper}
END
