#!/usr/bin/env sh

paru -S --noconfirm --disable-download-timeout \
  feh \
  imagemagick \
  ueberzug \
  tumbler \
  openjpeg2 \
  libjxl \
  librsvg \
  openexr \
  libwmf \
  libheif \
  mpv \
  libwebp \
  ffmpeg \
  ffnvcodec-headers \
  ffmpegthumbnailer \
  odt2txt \
  freetype2 \
  libgsf \
  gnome-epub-thumbnailer \
  evince \
  libxml2 \
  libcaca \
  djvulibre \
  ghostscript \
  pango \
  poppler \
  python-chardet \
  ;
printf "[VIRTUOSO]: Installed media tools\n"
