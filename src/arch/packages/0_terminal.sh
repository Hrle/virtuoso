#!/usr/bin/env sh

paru -S --noconfirm --disable-download-timeout \
  bash-completion \
  cod \
  fastfetch \
  talkfilters \
  zoxide \
  bat \
  exa \
  tree \
  ranger \
  lazygit \
  git-delta \
  fd \
  sd \
  sad \
  ripgrep \
  fzf \
  fzy \
  jq \
  rlwrap \
  dos2unix \
  atool \
  zip \
  mediainfo \
  lynx \
  speedtest-cli \
  procs \
  htop \
  powertop \
  inxi \
  duf \
  ncdu \
  glow \
  highlight \
  zk \
  unrar \
  dog \
  net-tools \
  godns-bin \
  renameutils \
  translate-shell \
  woeusb \
  ;
cat <<END >/lib/systemd/user/godns.service
[Unit]
Description=GoDNS Service
After=network.target

[Service]
ExecStart=godns -c %h/.config/godns/config.json
Restart=always
KillMode=process
RestartSec=2s
END
cat <<END >/lib/systemd/user/godns.timer
[Unit]
Description=GoDNS Timer

[Timer]
OnCalendar=hourly

[Install]
WantedBy=timers.target
END
systemctl enable --user --now godns.service
printf "[VIRTUOSO]: Installed terminal tools\n"
